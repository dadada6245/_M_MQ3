#ifndef _MAKER_MQ3_
#define _MAKER_MQ3_

extern double R0;

/*
 * Note: Returns the voltage read from the sensor.
 *
 */
double maker_getSensorVolt(uint8_t analogPin);

/*  
 *
 * Note: Returns sensor resistance in air.
 *
 */
double maker_getRSAir(uint8_t analogPin);

/*
 *
 * Note: Returns sensor resistance at 0.4mg/L of alcohol in the clean air
 * which is later used in function maker_getRatio
 */
double maker_getEnvironmentR0(uint8_t analogPin);

/*  
 *
 * Note: Returns ratio RSAir/R0.
 *
 */
double maker_getRatio(uint8_t analogPin);

#endif
