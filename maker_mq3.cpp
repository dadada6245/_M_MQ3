/*
 * Library: maker_mq3.h
 *
 * Organization: MakerRobotics
 * Autors: Katarina Stankovic.
 * 
 * Date: 17.11.2017.
 * Test: Arduino UNO
 * 
 * Note:
 * User must call maker_getEnvironmentR0() function out of loop and 
 * in air before exposing sensor to concentrated gas and before
 * calling maker_getRatio()
 */
#include "Arduino.h"
#include "maker_mq3.h"
#include "stdint.h"

double R0;

static uint8_t _getSensorValue(uint8_t analogPin)
{
	 return analogRead(analogPin);
}
double maker_getSensorVolt(uint8_t analogPin)
{
	return (double)_getSensorValue(analogPin)/1024*5.0;
}
double maker_getRSAir(uint8_t analogPin)
{
	double SensorVolt=maker_getSensorVolt(analogPin);
	return (5.0-SensorVolt)/SensorVolt;
}

double maker_getEnvironmentR0(uint8_t analogPin)
{
	double sensorValue=0;
	for(int x = 0 ; x < 100 ; x++)
	{
            sensorValue = sensorValue + analogRead(analogPin);
	}
    sensorValue = sensorValue/100.0;

    double sensor_volt = sensorValue/1024*5.0;
    double RS_air = (5.0-sensor_volt)/sensor_volt; 
    R0 = RS_air/60.0; 
}

double maker_getRatio(uint8_t analogPin)
{
	 return maker_getRSAir(analogPin)/R0; 
}
